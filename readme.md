
# Polen

Polen is a wordpress plugin to manage a collection of resources to share with a team (like volunteers). With this plugin you can build a mediakit page easy to mantain and share. 

Each resource has title, description, featured image and links.

## Getting started
1. Install and activate Polen plugin.
2. Go to "Recursos" at the Wordpress'dashboard.
3. Create a new resources.
4. Create a new page to show the resources. Also you can use a blog post.
5. Insert a shortcode block with the following code: `[polen_grid]`.

## Insert into a page using a shortcode
To insert the content in a page, you should use the shorcode `[polen_grid]`. 


### Add a title
Use **title** attribute to add a custom title to the resources grid.
 ```
 [polen_grid title="Fundraising resources"]
 ``` 

### Custom order

Use **order** and **orderby** attributes to change the order of the items.

 ```
 [polen_grid order="asc" orderby="title"]
 ``` 

* **order:** use `"asc"` to ascending order or `"desc"` to descending order.
* **orderby:** you can order by `"title"`, creation `"date"`, `"modified"` date.


### Filter resources
With **filter** attribute you can filter the posts to show.
 ```
 [polen_grid filter="Presentation"]
 ``` 

 ## Screenshots

 ### Editor view

![Screenshot of the Wordpress editor to create a new resource ](screenshots/polen-resource-editor.png)

 ### Card view

![A visual card that represents a collection of resources to share](screenshots/polen-resource-card.png)