FILENAME="polen.pot"
FILEPATH="languages/$FILENAME"
INCLUDES="*.php"

wp i18n make-pot . $FILEPATH --ignore-domain --include=$INCLUDES
poedit $FILEPATH
